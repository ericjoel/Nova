var contact = (function () {
    return {
        init: function () {
            this.submitContactHandler();
        },
        submitContactHandler: function () {
            $("form[name='contactForm']").submit(
                function () {
                    $(".progress-contact").show();
                    $( "form[name='contactForm']>button" ).prop( "disabled", true );
                    $.when(contactBase.sendEmail($("form[name='contactForm']"))).then(function (data) {
                        if (data) {
                            $(".progress-contact").hide();
                            $( "form[name='contactForm']>button" ).prop( "disabled", false );
                            if (data.error)
                                Materialize.toast(data.error, 4000, 'toast-error')
                            else
                                Materialize.toast(data.result, 4000, 'toast-success')
                        } 
                    });
                    return false;
                });
        }
    };
}());

$(function() {
    contact.init();
});