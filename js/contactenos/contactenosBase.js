var contactBase = (function () {
    return {
        init : function () {
              
        },
        sendEmail : function (form) {
            
            var data = $(form).serialize();
            
            return $.ajax({
                type: "POST",
                url: "php/contact/sendEmail.php",
                data: data,
                dataType: "json",
                success: function (data) {
                    $(".progress-contact").hide();
                    $( "form[name='contactForm']>button" ).prop( "disabled", false );
                },
                error: function (data) {
                    $(".progress-contact").hide();
                    $( "form[name='contactForm']>button" ).prop( "disabled", false );
                }
            });    
        }
    };
}());