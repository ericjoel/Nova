<?php
    header('Content-Type: application/json');
    error_reporting(E_ERROR | E_PARSE);
    $aResult = array();

    if (!isset($_POST['first_name']) || 
        strlen($_POST['first_name']) > 50 || 
        strlen($_POST['first_name']) < 1)
    {
        if (!isset($_POST['first_name']) || (strlen($_POST['first_name']) < 1) )
            $aResult['error'] = 'El campo nombre es requerido';
        else if (strlen($_POST['first_name']) > 50)
            $aResult['error'] = 'La longitud máxima del nombre es 50';              
    }
    else if(!isset($_POST['last_name']) || 
            strlen($_POST['last_name']) > 50 || 
            strlen($_POST['last_name']) < 1)
    {
        if (!isset($_POST['last_name']) || (strlen($_POST['last_name']) < 1) )
            $aResult['error'] = 'El campo apellido es requerido';
        else if (strlen($_POST['last_name']) > 50)
            $aResult['error'] = 'La longitud máxima del apellido es 50';   
    }
    else if(!isset($_POST['email']) || 
            (strlen($_POST['email']) > 50)|| 
            (strlen($_POST['email']) < 1))
    {
        if (!isset($_POST['email']) || (strlen($_POST['email']) < 1) )
            $aResult['error'] = 'El campo correo es requerido';
        else if (strlen($_POST['email']) > 50)
            $aResult['error'] = 'La longitud máxima del correo es 100';   

    }
    else if(!isset($_POST['phone']) || 
            strlen($_POST['phone']) > 50 || 
            strlen($_POST['phone']) < 1)
    {
        if (!isset($_POST['phone']) || (strlen($_POST['phone']) < 1) )
            $aResult['error'] = 'El campo teléfono es requerido';
        else if (strlen($_POST['phone']) > 50)
            $aResult['error'] = 'La longitud máxima del teléfono es 100';          
    }
    else if(!isset($_POST['message'])|| 
            strlen($_POST['message']) > 2000 || 
            strlen($_POST['message']) < 1)
    {
        if (!isset($_POST['message']) || (strlen($_POST['message']) < 1) )
            $aResult['error'] = 'El campo mensaje es requerido';
        else if (strlen($_POST['phone']) > 50)
            $aResult['message'] = 'La longitud máxima del mensaje es 2000';   
    }

    if(!isset($aResult['error']) ) {
        include("messageTemplate.php");
        
        $message = str_replace("{NAME}",$_POST['first_name'],$message);
        $message = str_replace("{LAST_NAME}",$_POST['last_name'],$message);
        $message = str_replace("{EMAIL}",$_POST['email'],$message);
        $message = str_replace("{PHONE}",$_POST['phone'],$message);
        $message = str_replace("{MESSAGE_CLIENT}",$_POST['message'],$message);
        $account="eric.contreras93@gmail.com";
        $password="3ric.contreras93";
        $to="corporacionneycef@gmail.com";
        $from="eric.contreras93@gmail.com";
        $from_name="Contácto Neycef";
        $msg=$message; // HTML message
        $subject="Contácto Neycef";
        /*End Config*/

        include("../phpmailer/class.phpmailer.php");
        

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth= true;
        $mail->Port = 465; // Or 587
        $mail->Username= $account;
        $mail->Password= $password;
        $mail->SMTPSecure = 'ssl';
        $mail->From = $from;
        $mail->FromName= $from_name;
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $msg;
        $mail->addAddress($to);
        $mail->AddAddress('c1sar90@gmail.com', 'Cesar Contreras');
        $mail->AddAddress('eric.contreras93@gmail.com', 'Eric Contreras');

        if(!$mail->send()){
            $aResult['error'] = "No se pudo enviar el correo";            
        }
        else{
            $aResult['result'] = "EL correo se ha enviado correctamente.";
        }
    }

    echo json_encode($aResult);
?>