<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <title>Neycef</title>
    <link rel="icon" type="image/png" href="img/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="theme/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link rel="stylesheet" href="css/font-awesome.min.css" rel="stylesheet" />

    <link href="css/main.css" type="text/css" rel="stylesheet" />
</head>

<body>
    <header class="navbar-fixed">
        <nav class="blue" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="#inicio" class="brand-logo">
                    <div>
                        <img class="responsive-img" src="img/logo-nav.png" alt="Neycef Logo">
                    </div>
                </a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="#nosotros">Nosotros</a></li>
                    <li><a href="#servicios">Servicios</a></li>
                    <li><a href="#contactenos">Contacto</a></li>
                    <li><a href="#ubicanos">Ubicanos</a></li>
                </ul>

                <ul id="nav-mobile" class="side-nav">
                    <li><a href="#nosotros">Nosotros</a></li>
                    <li><a href="#servicios">Servicios</a></li>
                    <li><a href="#contactenos">Contacto</a></li>
                    <li><a href="#ubicanos">Ubicanos</a></li>
                </ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            </div>
        </nav>
    </header>
    <main>
        <section id="inicio" class="parallax-container section scrollspy">
            <div class="section no-pad-bot ">
                <div class="container">
                    <br>
                    <br>
                    <h2 class="header center blue white-text ">Corporación Neycef</h2>
                    <div class="row center">
                        <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
                    </div>
                    <div class="row center">
                        <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light blue">Get Started</a>
                    </div>
                    <br>
                    <br>

                </div>
            </div>
            <div class="parallax"><img src="img/background2.png"></div>
        </section>

        <section id="servicios" class="section scrollspy container">
            <div class="row">
                <div class="col s12 m6 l4">
                    <div class="card-panel card-service white hoverable">
                        <div class="col s12 header-card">
                            <br>
                            <h1 class="center-align"><i class="fa fa-print"></i></h1>
                        </div>
                        <div class="col s12">
                            <h5 class="center-align grey-text text-darken-2 title-service">Impresiones</h5>
                            <p class="center-align grey-text">Impresiones color o blanco y negro de óptima calidad mediante impresoras Laser, Impresión de transparencias color o blanco y negro</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel card-service white hoverable" style="height:340px">
                        <div class="col s12 header-card">
                            <br>
                            <h1 class="center-align"><i class="fa fa-desktop"></i></h1>
                        </div>
                        <div class="col s12">
                            <h5 class="center-align grey-text text-darken-2 title-service">Trabajos por Computadora</h5>
                            <p class="center-align grey-text">Elaboración de trabajos por computadora, incluyendo elaboración de síntesis curricular, trípticos, trabajos especiales, etc.</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel card-service white hoverable" style="height:340px">
                        <div class="col s12 header-card">
                            <br>
                            <h1 class="center-align"><i class="fa fa-keyboard-o"></i></h1>
                        </div>
                        <div class="col s12">
                            <h5 class="center-align grey-text text-darken-2 title-service">Digitalización</h5>
                            <p class="center-align grey-text">Digitalización de documentos ya sea por escaneos o transcripciones</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel card-service white hoverable" style="height:340px">
                        <div class="col s12 header-card">
                            <br>
                            <h1 class="center-align"><i class="fa fa-fax"></i></h1>
                        </div>
                        <div class="col s12">
                            <h5 class="center-align grey-text text-darken-2 title-service">Fax</h5>
                            <p class="center-align grey-text">Servicio de envío y recepción de correos electrónicos, fax</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel card-service white hoverable" style="height:340px">
                        <div class="col s12 header-card">
                            <br>
                            <h1 class="center-align"><i class="fa fa-clone"></i></h1>
                        </div>
                        <div class="col s12">
                            <h5 class="center-align grey-text text-darken-2 title-service">Fotocopias</h5>
                            <p class="center-align grey-text">Fotocopiado de Documentos por mayor o menor, Blanco y Negro o Color.</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card-panel card-service white hoverable" style="height:340px">
                        <div class="col s12 header-card">
                            <br>
                            <h1 class="center-align"><i class="fa fa-folder-open"></i></h1>
                        </div>
                        <div class="col s12">
                            <h5 class="center-align grey-text text-darken-2 title-service">Carpetas</h5>
                            <p class="center-align grey-text">Servicios especiales en apoyo técnico tendientes a obtener citas de bancos, saime, rree, transporte,llenado de planillas por internet, preparación de carpetas para cencoex cupo de viajero, remesas familiares, etc</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section scrollspy" id="nosotros">

            <div class="parallax-container valign-wrapper">
                <div class="no-pad-bot full-width">
                    <div class="container">
                        <div class="row center white-text">
                            <h2><i class="mdi-content-send white-text"></i></h3>
                            <h2 class="header col s12 white-text blue">Acerca de nosotros</h2>
                        </div>
                    </div>
                </div>
                <div class="parallax"><img src="img/background3.jpg" alt="Unsplashed background img 3"></div>
            </div>

            <div class="row">
                <div class="container">
                    <div class="col s12 center">
                        <p class="left-align flow-text light">Corporación Neycef 2020, C.A. es una empresa formada con el propósito de satisfacer a un mercado en avance, nos dedicamos a la venta de productos o servicios usando varios medios de cómo llegar a nuestros clientes ya sea a través de nuestro local o ventas a través de medios electrónicos, estamos enfocados en la venta de equipos de tecnología, y a su vez en una gran variedad de productos típicos de un bazar como es el de regalos, las computadoras y todo lo que esto implica. Somos comercializadores de las marcas más reconocidas, para brindarles a nuestros clientes una satisfacción total.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="ubicanos" class="section scrollspy">
            <div class="parallax-container valign-wrapper">
                <div class="section no-pad-bot full-width">
                    <div class="container">
                        <div class="row center">
                            <h2 class="header col s12 white-text">Ubícanos</h2>
                        </div>
                    </div>
                </div>
                <div class="parallax"><img src="img/background3.jpg"></div>
            </div>
            <div class="container">
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <div id="map-canvas" class="map-canvas form-group"></div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <span class="card-title blue-text">Dirección</span>
                                <p>Estamos ubicados en el Centro de Caracas</p>
                                <p>En la Avenida Urdaneta, Edificio Caracas, Piso 2, Oficina 4</p>
                                <p>Entre las Esquinas Veroes a Ibarras (Av. Norte y Av. Norte 1)</p>
                                <p><strong>Punto de Referencia: </strong>Dorsay</p>
                            </div>
                            <div class="col s12 m6 l6">
                                <span class="card-title blue-text">Horario de Atención</span>
                                <p>Corrido de 08:00 a 18:00 Horas</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="contactenos" class="section scrollspy">
            <div class="parallax-container valign-wrapper">
                <div class="section no-pad-bot full-width">
                    <div class="container">
                        <div class="row center white-text">
                            <h2 class="header col s12 light">Contáctenos</h2>
                        </div>

                    </div>
                </div>
                <div class="parallax"><img src="img/background4.jpg" alt="Unsplashed background img 3"></div>
            </div>

            <div class="container row">
                <div class="col s12 m6">
                    <h4 class="blue-text light">
						Información
					</h4>
                    <div class="row">
                        <div class="col s12">
                            <span class="card-title blue-text">Dirección</span>
                            <p>Estamos ubicados en el Centro de Caracas</p>
                            <p>En la Avenida Urdaneta, Edificio Caracas, Piso 2, Oficina 4</p>
                            <p>Entre las Esquinas Veroes a Ibarras (Av. Norte y Av. Norte 1)</p>
                            <p><strong>Punto de Referencia: </strong>Dorsay</p>
                        </div>
                        <div class="col s12">
                            <br>
                        </div>
                        <div class="col s12 m6 l6">
                            <span class="card-title blue-text">Horario de Atención</span>
                            <p>Corrido de 08:00 a 18:00 Horas</p>
                        </div>
                        <div class="col s12 m6 l6">
                            <span class="card-title blue-text">Teléfonos</span>
                            <p>0212-563.13.68</p>
                            <p>0212-637.97.63</p>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6">
                    <h4 class="blue-text light">
						Contácto
					</h4>
                   <div class="progress progress-contact" style="display:none;">
                        <div class="indeterminate"></div>
                    </div>
                    <div class="row">
                        <form class="col s12" name="contactForm">
                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input id="first_name" minlength="2" required name="first_name" length="50" maxlength="50" type="text" class="validate" required data-errormessage-value-missing="Debe llenar este campo">
                                    <label for="first_name">Nombre</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input id="last_name" name="last_name" required data-errormessage-value-missing="Debe llenar este campo" length="50" maxlength="50" type="text" class="validate">
                                    <label for="last_name">Apellido</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" name="email" type="email" length="50" maxlength="100" class="validate" required data-errormessage-value-missing="Debe llenar este campo" data-errormessage-type-mismatch="El correo es inválido">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="phone" name="phone" length="50" required data-errormessage-value-missing="Debe llenar este campo" maxlength="50" type="text" class="validate">
                                    <label for="phone">Teléfono</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="message" name="message" required data-errormessage-value-missing="Debe llenar este campo" minlength="10" length="2000" maxlength="2000" class="materialize-textarea"></textarea>
                                    <label for="message">Mensaje</label>
                                </div>
                            </div>
                            <div class="row">
                                <button class="btn btn-large waves-effect waves-light right blue" type="submit" name="action">
                                    Contactar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </main>


    <footer class="page-footer blue">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Corporación Neycef 2020 C.A.</h5>
                    <p class="grey-text text-lighten-4">Rif: J-552222.</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2015 Dynamic Devs
                <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
        </div>
    </footer>

    <!--  Scripts-->
    <script src="js/jquery.js"></script>
    <script src="js/civem.js"></script>
    <script src="theme/js/materialize.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="js/index.js"></script>
    <script src="js/base.js"></script>
    <script src="js/contactenos/contactenosBase.js"></script>
    <script src="js/contactenos/contactenos.js"></script>

</body>

</html>